import './App.scss';
import React, {Component} from "react";
import Jimp from 'jimp';
import Dropzone from 'react-dropzone';
import {triggerBase64Download} from 'react-base64-downloader';
import lazadalogo from './img/lazada.png';
import pgmalllogo from './img/pgmall.png';
import qoo10logo from './img/Qoo10.png';
import shopeelogo from './img/shopee.png';

//keeping the base logo img for testing purpose
// import lazadalogo from './img/greylazada.png';
// import pgmalllogo from './img/greypgmall.png';
// import qoo10logo from './img/greyQoo10.png';
// import shopeelogo from './img/greyshopee.png';

class ContentFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFile: null,
            baseLogo: null,
            logoName: null,
            logoResult: [],
            files: [],
        }

        this.onDrop = (imageFile) => {
            const fileArr: any = [];
            for (const file of imageFile) {
                fileArr.push(URL.createObjectURL(file));
            }
            this.setState({files: fileArr});
        };
    }

    RadioOnChangeValue = (event) => {
        switch (event.target.value) {
            case 'lazada':
                this.setState({
                    baseLogo: lazadalogo,
                    logoName: 'lazada'
                })
                break;
            case 'pgmall':
                this.setState({
                    baseLogo: pgmalllogo,
                    logoName: 'pgmall'
                })
                break;
            case 'qoo10':
                this.setState({
                    baseLogo: qoo10logo,
                    logoName: 'qoo'
                })
                break;
            case 'shopee':
                this.setState({
                    baseLogo: shopeelogo,
                    logoName: 'shopee'
                })
                break;
        }
    }

    //combine logo and base image
    generateLogoHandler = async () => {
        let logoResult: any = [];
        for (const blobFile of this.state.files) {
            const image = await Jimp.read(blobFile);
            await image.autocrop().contain(250, 100).quality(90).getBase64Async(Jimp.MIME_PNG);
            const baseLogo = await Jimp.read(this.state.baseLogo);

            switch (this.state.logoName) {
                case 'lazada':
                    logoResult.push(await baseLogo.composite(image, 20, 20).getBase64Async(Jimp.MIME_PNG));
                    break;
                case 'shopee':
                    logoResult.push(await baseLogo.composite(image, 24, 20).getBase64Async(Jimp.MIME_PNG));
                    break;
                case 'pgmall':
                    logoResult.push(await baseLogo.composite(image, 54, 20).getBase64Async(Jimp.MIME_PNG));
                    break;
                case 'qoo':
                    logoResult.push(await baseLogo.composite(image, 54, 20).getBase64Async(Jimp.MIME_PNG));
                    break;
            }
        }
        this.setState({logoResult: logoResult});
    }

    //preview logo on click
    removePreviewImg = (e) => {
        const filesArr = this.state.files;
        const index = filesArr.indexOf(e.target.currentSrc);
        if (index !== -1) {
            filesArr.splice(index, 1);
            this.setState({files: filesArr});
        }
    }

    render() {
        const thumbs = this.state.files.map(file => (
            <div className="thumb" key={file.name}>
                <div className="thumbInner">
                    <img
                        src={file}
                        className="img"
                        onClick={this.removePreviewImg}
                    />
                    <p>{file.name}</p>
                </div>
            </div>
        ));

        const generateNewLogo = this.state.logoResult.map(newLogo => (
            <ul>
                <div>
                    <img src={newLogo}/>
                    <button onClick={() => triggerBase64Download(newLogo, `des_generate_logo_${Date.now()}`)}>Download
                    </button>
                </div>
            </ul>
        ));

        return (
            <div className="contentFilter">
                <div className="dropZoneContainer">
                    <Dropzone onDrop={this.onDrop}
                              accept="image/png, image/jpeg, image/jpg"
                    >
                        {({getRootProps, getInputProps, isDragActive, isDragReject}) => (
                            <section className="container">
                                <div {...getRootProps({className: 'dropzone'})}>
                                    <input id="test" type="file" {...getInputProps()}
                                    />
                                    {!isDragActive && 'Drag and drop a file to upload!'}
                                    {isDragReject && "File type not accepted, sorry!"}
                                </div>
                                {this.state.files.length>0 &&
                                <p>Selected Images</p>
                                }
                            </section>
                        )}
                    </Dropzone>
                    {this.state.files.length > 0 &&
                    <div className="logoPreviewContainer">
                        <aside className="thumbsContainer">
                            {thumbs}
                        </aside>
                    </div>
                    }
                </div>
                <div onChange={this.RadioOnChangeValue} className="radioBtnContainer">
                    <label><input type="radio" value="lazada" name="chainRadio"/> Lazada</label>
                    <label><input type="radio" value="shopee" name="chainRadio"/> Shopee</label>
                    <label><input type="radio" value="pgmall" name="chainRadio"/> PGMall</label>
                    <label><input type="radio" value="qoo10" name="chainRadio"/> Qoo10</label>
                </div>
                <div className="generateLogoContainer">
                    <button onClick={this.generateLogoHandler}>Generate Logo</button>
                </div>
                <div>
                    {generateNewLogo}
                </div>
            </div>
        );
    }
}
export default ContentFilter;
