import logo from './logo.svg';
import './App.scss';
import ContentFilter from "./ContentFilter";

function App() {
    return (
        <div className="App">
            <ContentFilter/>
        </div>
    );
}

export default App;
